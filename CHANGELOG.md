# [3.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.7.4...v3.0.0) (2024-07-24)


### Features

* run_probes - add timeout for each check ([af7dc06](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/af7dc067a7174cdb0f5a55813a968629e8679658))


### BREAKING CHANGES

* config structure change + new required option

## [2.7.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.7.3...v2.7.4) (2024-05-30)


### Bug Fixes

* **check_saml:** perform initial redirect if needed, not based on hostname ([9c75388](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/9c75388cc4447accdf0a7fbd903b5f8d91d2b112))

## [2.7.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.7.2...v2.7.3) (2024-04-24)


### Bug Fixes

* correct ON CONFLICT in oracle2postgresql ([f4282ea](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/f4282ea416ca62b622f84344a786a2bb8abee458))

## [2.7.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.7.1...v2.7.2) (2024-04-23)


### Bug Fixes

* lowercase table name in oracle2postgresql ([73e6f0a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/73e6f0ae77da64e9c0661506749a9b35aabf95a9))

## [2.7.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.7.0...v2.7.1) (2024-04-23)


### Bug Fixes

* use server_default in oracle2postgresql ([f0465bd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/f0465bd4d798e542ba9ef269556a12ab23c552d1))

# [2.7.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.6.0...v2.7.0) (2024-04-22)


### Features

* script for oracle db to postgresql sync ([8ad295f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/8ad295fce94da4794eeda38999c2167929d00864))

# [2.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.5.4...v2.6.0) (2024-02-15)


### Features

* satosa oidc probe ([f5cb1df](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/f5cb1df28b71cb78a1ac69ab5db39a923acc6925))

## [2.5.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.5.3...v2.5.4) (2024-02-12)


### Bug Fixes

* user token types calculation logic ([669e2bb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/669e2bb5908e7c3c6dccf984818ea01e84defd73))

## [2.5.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.5.2...v2.5.3) (2024-02-07)


### Bug Fixes

* provide flask app context without pi shell ([1757c6b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/1757c6b316092d2062fb920740e1b4cb3be2e62c))

## [2.5.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.5.1...v2.5.2) (2024-02-05)


### Bug Fixes

* output without metrics ([7b0fda7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/7b0fda7d6ac85546eaf72c2aaacdfd24de4e1d9b))

## [2.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.5.0...v2.5.1) (2024-02-05)


### Bug Fixes

* main cannot have args ([6b58c32](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/6b58c32e0756ce6e00787c21e48363d6bf2a625a))

# [2.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.4.0...v2.5.0) (2024-02-05)


### Features

* arguments handling with argparse ([4f068a9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/4f068a9652a119a8ad54e52c5e82938b754d4086))

# [2.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.3.0...v2.4.0) (2024-02-01)


### Features

* run_probes outputs in check_mk format with metrics to graphs ([26da3f3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/26da3f35c851664116b6402a7c856744b5bbb26e))

# [2.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.2.2...v2.3.0) (2024-01-25)


### Bug Fixes

* modify filtering condition for usable tokens ([ea1707b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/ea1707b507ce2d494abefa43dd3202b927689268))


### Features

* sync script for privacyidea tokens to perun ([aa76214](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/aa762141e6fa1000f016c3adee33e66e62de7086))

## [2.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.2.1...v2.2.2) (2024-01-19)


### Bug Fixes

* **check_saml:** adapt security text check, remove image ([ec4ca60](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/ec4ca60834e1995db8d21af5900e790875db8e14))

## [2.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.2.0...v2.2.1) (2024-01-09)


### Bug Fixes

* **check_saml:** follow all redirects on ProxyIdP ([f7c8fc7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/f7c8fc768c4a989f523da77777cc4b28b210786f))

# [2.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.1.0...v2.2.0) (2024-01-05)


### Features

* check_saml.py security text support ([36ce42d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/36ce42d33d6410944881203e18f2dc97b3943599))

# [2.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v2.0.0...v2.1.0) (2024-01-02)


### Features

* add basic oidc check option ([533b390](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/533b3906665bedcc009cf5491b1f47d237e05701))
* add check saml example to readme ([2a82570](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/2a82570cd13cd174658b9d3abe486b63d56f9a1e))

# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.11.0...v2.0.0) (2023-09-08)


### Bug Fixes

* make check_pgsql command work ([3375756](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/3375756a565fa57518a840fd26f299b03d2903d2))


### chore

* move check_syncrepl_extended to the ldap extra ([898aff3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/898aff301c8f92ba48a528e739a288f59a8d9b18))


### BREAKING CHANGES

* usage of check_syncrepl_extended now requires installing with [ldap]

# [1.11.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.10.0...v1.11.0) (2023-09-08)


### Features

* check_pgsql ([76d83a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/76d83a526628daf54cb88eb672c3096e4421d25a))

# [1.10.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.9.2...v1.10.0) (2023-08-24)


### Features

* check_privacyidea monitoring probe ([bb00c80](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/bb00c80ff7c4329c3463670cff05b3b2998ca7dd))

## [1.9.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.9.1...v1.9.2) (2023-08-23)


### Bug Fixes

* check_saml totp form detection, response message with --skip_logout_check argument ([13ba531](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/13ba531fd9b4e462fa37235862337c82eafa9cde))

## [1.9.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.9.0...v1.9.1) (2023-08-23)


### Bug Fixes

* check_php_syntax to work with containers ([5cfec90](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/5cfec908071110ae676cef899014ee1e8ec24481))

# [1.9.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.8.5...v1.9.0) (2023-08-21)


### Features

* check_php_syntax ([0d9e162](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/0d9e162433d54e72dfd6b19dd11c4da6715bc077))

## [1.8.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.8.4...v1.8.5) (2023-08-10)


### Bug Fixes

* double space after probe name ([c2fc77d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/c2fc77ddb2c75991b19a9eb533994eb8c973654b))

## [1.8.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.8.3...v1.8.4) (2023-08-07)


### Bug Fixes

* containers argument format in check_docker probe ([987ab12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/987ab12a833f1e53f026e0de8103aa5dbb6c47db))

## [1.8.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.8.2...v1.8.3) (2023-08-06)


### Bug Fixes

* run_probes support for probes executed without arguments and flags ([60364d4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/60364d4e0e24166f0f41821304510a3c2cf3720d))

## [1.8.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.8.1...v1.8.2) (2023-07-18)


### Bug Fixes

* **deps:** move ldap3 to extras ([1795b12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/1795b1255e6b772e878b600db72099a7b745998a))

## [1.8.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.8.0...v1.8.1) (2023-07-12)


### Bug Fixes

* support for PyYAML 5.4 ([fb0cf11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/fb0cf119ec7614cbf88df58d390075772819157f))

# [1.8.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.7.1...v1.8.0) (2023-07-12)


### Features

* run_probes support for command line flags and printing stderr, entry_points support ([55edad8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/55edad8e344164302aadc920ac9957987218d054))

## [1.7.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.7.0...v1.7.1) (2023-06-28)


### Bug Fixes

* dependencies from pypi, not GitLab ([bf643a6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/bf643a6657428f714ce44c16ff7fc23b87a79708))

# [1.7.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.6.0...v1.7.0) (2023-06-20)


### Features

* script to execute multiple monitoring probes ([d39bfa3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/d39bfa3301794d18ea486e3a90acff9b2998b944))

# [1.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.5.0...v1.6.0) (2023-06-15)


### Features

* extend check_saml with logout ([aa218b5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/aa218b581ad6c35319617a3479b49863c6c385e0))

# [1.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.4.0...v1.5.0) (2023-06-14)


### Features

* add check for nginx status ([dcce8d6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/dcce8d6ead873b9f12dc8499b21c7e2f010629ae))

# [1.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.3.0...v1.4.0) (2023-06-07)


### Features

* check_exabgp_propagation, check_dockers, webserver_availability, check_syncrepl ([5106f36](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/5106f3656227a02d8d17ac5fa3fb819b9bdf75fd))

# [1.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.2.0...v1.3.0) (2023-06-07)


### Features

* check LDAP availability ([ac5eebd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/ac5eebd95cdb195f03df5b04eb137d771e793647))

# [1.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.1.0...v1.2.0) (2023-06-06)


### Features

* script calling non-python monitoring scripts ([159b00b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/159b00b5e0707306a1e1442d4dcdf3018587dcbc))
* script to check RPC availability ([ae1c2d2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/ae1c2d24add1804a911153d8efc7db2e9f8e8ea3))

# [1.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/compare/v1.0.0...v1.1.0) (2023-04-11)


### Features

* check_saml, check_user_logins ([72fa5fb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/72fa5fb047c76468834b2a148fa718fc6e5484c5))

# 1.0.0 (2023-03-31)


### Bug Fixes

* correct regular expression syntax ([6c28754](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/6c2875413f3cc5bcc2d1477639cd67250f60d019))


### Features

* Added metadata_expiration.py script ([cac41ad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/cac41ad950e66b755f8c768c11cff82c2dad4aa7))
* check_mongodb monitoring probe, restructuralization, prepared for pypi release ([145227a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/145227a4ba7efd2a17633f2fb90f2377c300a5c1))
* configuration file instead of command line args in mariadb probe ([c44df19](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/c44df19e6a630443b71dedbdbbe76fe4265acef7))
* print_docker_versions.py, run_version_script.py scripts ([7eb461b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/perun-proxy-utils/commit/7eb461b3742f3a3461e2733b51e53c55252dfbbe))


### BREAKING CHANGES

* only one command line argument is passed - path to the configuration file
